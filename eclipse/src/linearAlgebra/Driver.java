package linearAlgebra;

public class Driver {
	public static void main(String[] args) {
		Vector3d v1 = new Vector3d(1,2,3);
		Vector3d v2 = new Vector3d(4,5,6);
		
		System.out.println("Vector 1");
		System.out.println(v1.getX()+" "+v1.getY()+" "+v1.getZ());
		System.out.println("Magnitude Of Vector 1");
		System.out.println(v1.getMagnitude());
		
		System.out.println();
		
		System.out.println("Vector 2");
		System.out.println(v2.getX()+" "+v2.getY()+" "+v2.getZ());
		System.out.println("Magnitude Of Vector 2");
		System.out.println(v2.getMagnitude());
		
		System.out.println();
		
		System.out.println("Dot Product of Vector 1 and 2");
		System.out.println(v1.getDotProduct(v2));
		
		System.out.println();
		
		System.out.println("Added the two vectors");
		Vector3d v3 = v1.add(v2);
		System.out.println(v3.getX()+" "+v3.getY()+" "+v3.getZ());
	}
}
