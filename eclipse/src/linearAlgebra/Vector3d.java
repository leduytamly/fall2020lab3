/* Le Duytam Ly
 * 1734119*/

package linearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	/**
	 * Parameterized Constructor for Vector3d
	 * @param x value of x
	 * @param y value of y 
	 * @param z value of z
	 */
	public Vector3d(double x, double y, double z) {
		this.x=x;
		this.y=y;
		this.z=z;
	}
	
	/**
	 * Getter for x
	 * @return value of x
	 */
	public double getX() {
		return this.x;
	}
	
	/**
	 * Getter for y
	 * @return value of y
	 */
	public double getY() {
		return this.y;
	}
	
	/**
	 * Getter for z
	 * @return value of z
	 */
	public double getZ() {
		return this.z;
	}
	
	/**
	 * Calculates the magnitude of the vector
	 * @return Magnitude of the vector
	 */
	public double getMagnitude() {
		double magnitude = Math.sqrt((x*x)+(y*y)+(z*z));
		return magnitude;
	}
	
	/**
	 * Calculates the dot product of this vector and another
	 * @param v The other vector
	 * @return The dot product of the two vectors
	 */
	public double getDotProduct(Vector3d v) {
		double dotProduct = x * v.getX() + y * v.getY() + z * v.getZ();
		return dotProduct;
	}
	
	/**
	 * Adds a vector to another vector
	 * @param v The other vector
	 * @return A new vector which represents the sum of the two vector
	 */
	public Vector3d add(Vector3d v) {
		double sumX = x + v.getX();
		double sumY = y + v.getY();
		double sumZ = z + v.getZ();
		
		Vector3d sumVector = new Vector3d(sumX,sumY,sumZ);
		return sumVector;
	}
}
