/* Le Duytam Ly
 * 1734119*/

package linearAlgebra;

import static org.junit.jupiter.api.Assertions.*;



import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void AccessorsTest() {
		Vector3d v = new Vector3d(1,2.45,3);
		assertEquals(1,v.getX());
		assertEquals(2.45,v.getY());
		assertEquals(3,v.getZ());
	}
	
	@Test
	void MagnitudeTest() {
		Vector3d v = new Vector3d(1,2,3);
		assertEquals(3.741657387,v.getMagnitude(),.000001);
	}
	
	@Test
	void DotProductTest() {
		Vector3d v1 = new Vector3d(2,6,9);
		Vector3d v2 = new Vector3d(1,2.5,3);
		double result = v1.getDotProduct(v2);
		assertEquals(44.0,result);
	}

	@Test 
	void AddTest() {
		Vector3d v1 = new Vector3d(1,6,4.7);
		Vector3d v2 = new Vector3d(1,3,8);
		Vector3d vResult = v1.add(v2);
		assertEquals(2.0,vResult.getX());
		assertEquals(9.0,vResult.getY());
		assertEquals(12.7,vResult.getZ());
		
	}
}
